from PIL import Image

MEDIAN_COLOR = 127.5


class ImageProcessor:
    def __init__(self, img_path):
        self.imgPath = img_path
        self.img = self.__load_image().convert('L')

    def nodes_ends_count(self):
        nodes = 0
        ends = 0
        for x in range(self.img.width - 1):
            for y in range(self.img.height - 1):
                dot = self.img.getpixel((x, y))
                surr_0 = self.img.getpixel((x, y + 1)) < MEDIAN_COLOR
                surr_1 = self.img.getpixel((x + 1, y + 1)) < MEDIAN_COLOR
                surr_2 = self.img.getpixel((x + 1, y)) < MEDIAN_COLOR
                surr_3 = self.img.getpixel((x + 1, y - 1)) < MEDIAN_COLOR
                surr_4 = self.img.getpixel((x, y - 1)) < MEDIAN_COLOR
                surr_5 = self.img.getpixel((x - 1, y - 1)) < MEDIAN_COLOR
                surr_6 = self.img.getpixel((x - 1, y)) < MEDIAN_COLOR
                surr_7 = self.img.getpixel((x - 1, y + 1)) < MEDIAN_COLOR
                if dot < MEDIAN_COLOR:
                    total = 0
                    for black_pix in [surr_0, surr_1, surr_2, surr_3, surr_4, surr_5, surr_6, surr_7]:
                        if black_pix:
                            total += 1
                    if (surr_0 and surr_3 and surr_4 and surr_5) or (surr_4 and surr_7 and surr_0 and surr_1) or (
                            surr_2 and surr_5 and surr_6 and surr_7) or (surr_6 and surr_1 and surr_2 and surr_3):
                        nodes += 1
                    if total == 1:
                        ends += 1
        return nodes, ends

    def print_info(self):
        print(self.img.format)
        print(self.img.size)
        print(self.img.mode)

    def __load_image(self):
        return Image.open(self.imgPath)
