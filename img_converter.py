import os
import csv
from lib.image_processor import ImageProcessor

f_letter_to_1 = {'f': 'Ф (Ф)', 'p': 'Пи (π)', 'o': 'Омега (Ω)'}


def main():
    with open('assets/generated_nodes_ends.csv', 'w', newline='') as csvfile:
        fieldnames = ['1', 'Узловые', 'Концевые', '4']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        images = os.listdir('./images')
        o = []
        p = []
        f = []
        for img in images:
            if img.startswith('f'):
                f.append(img)
            elif img.startswith('omega'):
                o.append(img)
            elif img.startswith('pi'):
                p.append(img)
        prev_f_letter = ''
        for img in o + p + f:
            img_proc = ImageProcessor(f'./images/{img}')
            nodes, ends = img_proc.nodes_ends_count()
            if prev_f_letter != img[0]:
                writer.writerow({'1': f_letter_to_1.get(img[0]), 'Узловые': nodes, 'Концевые': ends})
                prev_f_letter = img[0]
            else:
                writer.writerow({'Узловые': nodes, 'Концевые': ends})


if __name__ == '__main__':
    main()
