import csv
import math
from functools import reduce

import matplotlib.pyplot as plt
from lib import image_processor as ip


def main():
    f_coordinates = []
    omega_coordinates = []
    pi_coordinates = []
    dicts_by_letter = {'Ф (Ф)': f_coordinates, 'Омега (Ω)': omega_coordinates, 'Пи (π)': pi_coordinates}
    with open('./assets/generated_nodes_ends.csv') as table:
        reader = csv.DictReader(table, fieldnames=['1', '2', '3', '4'])
        actual_dict = []
        for i, row in enumerate(reader):
            new_dict = dicts_by_letter.get(row.get('1'))
            if new_dict is not None:
                actual_dict = new_dict
            actual_dict.append({'x': int(row['2']), 'y': int(row['3'])})

    f_x = [obj['x'] for obj in f_coordinates]
    f_y = [obj['y'] for obj in f_coordinates]
    omega_x = [obj['x'] for obj in omega_coordinates]
    omega_y = [obj['y'] for obj in omega_coordinates]
    pi_x = [obj['x'] for obj in pi_coordinates]
    pi_y = [obj['y'] for obj in pi_coordinates]

    plt.scatter(f_x, f_y,
                c='blue', label='Ф', marker='>')
    plt.scatter(omega_x, omega_y,
                c='green', label='Ω', marker='^')
    plt.scatter(pi_x, pi_y,
                c='red', label='π', marker='<')

    f_center = class_center(f_x, f_y)
    omega_center = class_center(omega_x, omega_y)
    pi_center = class_center(pi_x, pi_y)
    plt.scatter(f_center.get('x'), f_center.get('y'),
                c='blue', label='Эталон класса Ф', marker='.')
    plt.scatter(omega_center.get('x'), omega_center.get('y'),
                c='green', label='Эталон класса Ω', marker='.')
    plt.scatter(pi_center.get('x'), pi_center.get('y'),
                c='red', label='Эталон класса π', marker='.')

    plt.xlabel('Кол-во узловых точек')
    plt.ylabel('Кол-во концевых точек')
    plt.title('Визуализация объектов в пространстве признаков')
    plt.legend()
    plt.savefig('plot.png')

    # pragma classification
    img_path = input('Введите имя файла:')
    img_processor = ip.ImageProcessor(img_path)
    nodes_and_ends = img_processor.nodes_ends_count()
    distance_to_f_center = euclide_distance(nodes_and_ends[0], nodes_and_ends[1], f_center.get('x'), f_center.get('y'))
    distance_to_omega_center = euclide_distance(nodes_and_ends[0], nodes_and_ends[1], omega_center.get('x'),
                                                omega_center.get('y'))
    distance_to_pi_center = euclide_distance(nodes_and_ends[0], nodes_and_ends[1], pi_center.get('x'),
                                             pi_center.get('y'))

    if distance_to_pi_center < distance_to_f_center and distance_to_pi_center < distance_to_omega_center:
        plt.scatter(nodes_and_ends[0], nodes_and_ends[1],
                    c='red', marker=',')
    elif distance_to_omega_center < distance_to_f_center and distance_to_omega_center < distance_to_pi_center:
        plt.scatter(nodes_and_ends[0], nodes_and_ends[1],
                    c='green', marker=',')
    elif distance_to_f_center < distance_to_pi_center and distance_to_f_center < distance_to_omega_center:
        plt.scatter(nodes_and_ends[0], nodes_and_ends[1],
                    c='blue', marker=',')
    else:
        print('two or more distances are equal')

    plt.savefig('new_plot.png')
    print(f'расстояние до центра Ф - {distance_to_f_center}\nрасстояние до центра Ω - {distance_to_omega_center}\n'
          f'расстояние до центра π - {distance_to_pi_center} ')


def min(values):
    ret = values[0]
    for val in values:
        if val < min:
            ret = val
    return ret


def euclide_distance(x_1, y_1, x_2, y_2):
    return math.sqrt((x_2 - x_1) ** 2 + (y_2 - y_1) ** 2)


def class_center(x_list, y_list):
    avg_x = reduce(lambda acc, cur: acc + cur, x_list) / len(x_list)
    avg_y = reduce(lambda acc, cur: acc + cur, y_list) / len(y_list)
    return {'x': avg_x, 'y': avg_y}


if __name__ == '__main__':
    main()
